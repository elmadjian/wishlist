#!/bin/bash

pip install --upgrade pip
pip install -r /app/requirements.txt

gunicorn app:api --bind 0.0.0.0:7000 --reload
