# wishlist API

A simple falcon-based REST API for a wishlist.


### Executing

Run the API as a docker image:
```
docker-compose up --build
```

Attaching an interactive terminal do the image, it is
also possible to run the tests, a available at the ``app/tests`` folder:
```
python -m unittest tests/tests.py
```

### Description

This API uses the following technologies:
* Falcon Web Framework (REST API)
* gunicorn (HTTP requests)
* PostgreSQL (databse)


### Use cases

##### GET
To retrieve, say, the item with id 3 of the wishlist:
```
curl -v http://0.0.0.0:7000/wishlist/3
```

To retrieve all the collection:
```
curl -v http://0.0.0.0:7000/wishlist
```

To get a random item from the list:
```
curl -v http://0.0.0.0:7000/wishlist/random
```

##### DELETE
To delete, say, the item with id 77 on the wishlist:
```
curl -X DELETE http://0.0.0.0:7000/wishlist/77
```

##### POST
To insert a new item named "Tamarindo" on the wishlist:
```
curl -X POST -d '{"title":"Tamarindo", "description":"It is a fruit from Africa", "owned": false, "link":"", "image":""}' -H 'Content-Type: application/json' http://0.0.0.0:7000/wishlist
```

Note that only the the "title" parameter is required to insert an item, but the request is only considered valid with all the fields (even if they are empty).

##### PUT
To update/modify an item with id 7 on the list:
```
curl -X PUT -d '{"title":"Carambola", "description":"Another fruit"}' -H 'Content-Type: application/json' http://0.0.0.0:7000/wishlist/7
```




