import falcon
import json
import marshmallow
from webargs import fields
from webargs.falconparser import use_args


class WishSchema(marshmallow.Schema):
    title = marshmallow.fields.Str(required=True)
    description = marshmallow.fields.Str()
    owned = marshmallow.fields.Bool()
    link = marshmallow.fields.Str()
    image = marshmallow.fields.Str()


class Wish():
    post_request_args = {
        "title": fields.Str(required=True),
        "description": fields.Str(required=False, default=None),
        "owned": fields.Bool(required=False, default=False),
        "link": fields.Str(required=False, default=None),
        "image": fields.Str(required=False, default=None)
    }


    def __init__(self, conn, db_service):
        self.conn = conn
        self.db_service = db_service
        self.db_service.create_schema()


    def _on_error(self, resp, error):
        output = {"status":False, "message":str(error), "data":None}
        resp.status = falcon.HTTP_500
        self._to_json(resp, output)


    def _on_clean(self, resp, db_reply, code):
        resp.status = code
        self._to_json(resp, db_reply)
        

    def _to_json(self, resp, reply):
        resp.set_header('Content-Type', 'application/json')
        resp.body = json.dumps(reply)


    def on_delete(self, req, resp, id):
        """Wishlist endpoint to delete a single item.
        ---
        description: Delete the item ID from the wishlist
                     if the ID does not exist, it returns
                     status message == 0
        responses:
            200:
                description: nothing
                schema: WishSchema
        """
        try:
            db_query = " ".join([
                "DELETE FROM wishlist",
                "WHERE wish_id = {}".format(id)
            ])
            db_reply = self.db_service.delete_query(self.conn, db_query)
            self._on_clean(resp, db_reply, falcon.HTTP_200)
        except Exception as e:
            self._on_error(resp, e)


    def on_get(self, req, resp, id):
        """Wishlist endpoint for a single item.
        ---
        description: Get the item ID from the wishlist
        responses:
            200:
                description: wishlist item to be returned
                schema: WishSchema
        """
        try:
            db_query = " ".join([
                "SELECT * FROM wishlist",
                "WHERE wish_id = {}".format(id) 
            ])
            db_reply = self.db_service.get_query(self.conn, db_query)
            self._on_clean(resp, db_reply, falcon.HTTP_200)
        except Exception as e:
            self._on_error(resp, e)

    
    def on_get_collection(self, req, resp):
        """Wishlist endpoint for all items.
        ---
        description: Get all the items in the wishlist
        responses:
            200:
                description: entire wishlist to be returned
                schema: WishSchema
        """
        try:
            db_query = "SELECT * FROM wishlist"
            db_reply = self.db_service.get_query(self.conn, db_query)
            self._on_clean(resp, db_reply, falcon.HTTP_200)
        except Exception as e:
            self._on_error(resp, e)


    def on_get_random(self, req, resp):
        """Wishlist endpoint for a random item.
        ---
        description: Get a single random item from the wishlist
        responses:
            200:
                description: wishlist item to be returned
                schema: WishSchema
        """
        try:
            db_query = "SELECT * FROM wishlist ORDER BY RANDOM() LIMIT 1"
            db_reply = self.db_service.get_query(self.conn, db_query)
            self._on_clean(resp, db_reply, falcon.HTTP_200)
        except Exception as e:
            self._on_error(resp, e)


    @use_args(post_request_args)
    def on_post_collection(self, req, resp, args):
        """Wishlist endpoint for inserting an item
        ---
        description: Insert a new item in the wishlist.
                     All the fields must be present (even if empty)
                     for a successful insertion
        responses:
            201:
                description: wishlist new item ID to be returned
                schema: WishSchema
        """
        try:
            db_query = " ".join([
                "INSERT INTO wishlist (title,description,owned,link,image)",
                "VALUES ('{}','{}','{}','{}','{}')".format(args['title'],
                                                      args['description'],
                                                      bool(args['owned']),
                                                      args['link'],
                                                      args['image']),
                "RETURNING wish_id"
            ])
            db_reply = self.db_service.upsert_query(self.conn, db_query)
            self._on_clean(resp, db_reply, falcon.HTTP_201)
        except Exception as e:
            self._on_error(resp, e)


    def on_put(self, req, resp, id):
        """Wishlist endpoint for updating an item
        ---
        description: Modify an existing item in the wishlist.                     
        responses:
            201:
                description: wishlist updated item ID to be returned
                schema: WishSchema
        """
        try:
            get_query = " ".join([
                "SELECT title,description,owned,link,image FROM",
                "wishlist WHERE wish_id = {}".format(id)
            ])
            get_reply = self.db_service.get_query(self.conn, get_query)
            if get_reply['status']:
                data = get_reply['data'][0]
                entries = ['title','description','owned','link','image']
                entry = {entries[i]: data[i] for i in range(len(data))}
                for k in req.media.keys():
                    if k in entry.keys():
                        entry[k] = req.media[k]
                    else:
                        raise Exception("{} is an invalid key".format(k)) 
                up_query = " ".join([
                    "UPDATE wishlist SET title='{}',".format(entry['title']),
                    "description='{}',".format(entry['description']),
                    "owned='{}',".format(entry['owned']),
                    "link='{}',".format(entry['link']),
                    "image='{}'".format(entry['image']),
                    "WHERE wish_id={} RETURNING wish_id".format(id)
                ])  
                up_reply = self.db_service.upsert_query(self.conn, up_query)
                self._on_clean(resp, up_reply, falcon.HTTP_201)  
        except Exception as e:
            self._on_error(resp, e)



            


    