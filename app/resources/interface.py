import falcon

class Interface():

    def __init__(self):
        pass

    
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.set_header('Content-Type', 'text/html')
        with open('data/html/interface.html', 'r') as f:
            resp.body = f.read()
