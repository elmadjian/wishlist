import psycopg2
import configparser
import os

path = os.path.dirname(os.path.realpath(__file__))
config = configparser.ConfigParser()
config.read(path + '/../conf/config.ini')

def connect():
    '''
    Connects to the wishlist DB (PostgreSQL)
    '''
    try:
        conn = psycopg2.connect(user=    config['db']['user'],
                                password=config['db']['password'],
                                host=    config['db']['host'],
                                port=    '5432',
                                database=config['db']['dbname'])
        print(">>> Connected to the DB.")
        return conn
    except (Exception, psycopg2.Error) as e:
        print(">>> Error while trying to connect to the DB:", e)
       

def create_schema():
    '''
    Creates the required schema for the wishlist table
    '''
    command = '''
              CREATE TABLE IF NOT EXISTS wishlist (
                wish_id     SERIAL PRIMARY KEY,
                title       VARCHAR (50) NOT NULL,
                description VARCHAR (250),
                owned       BOOLEAN NOT NULL DEFAULT FALSE,
                link        VARCHAR (150),
                image       VARCHAR (150)
             )
             '''
    try:
        conn = connect()
        cursor = conn.cursor()
        cursor.execute(command)
        conn.commit()
        print(">>> Table wishlist is available.")
    except (Exception, psycopg2.Error) as e:
        print(">>> Error while trying to create table wishlist:",e)
    finally:
        if conn is not None:
            cursor.close()
            conn.close()


def delete_query(conn, query):
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        return {"status": True, "message": cursor.statusmessage, "data": None}
    except psycopg2.Error as e:
        conn.rollback()
        return {"status": False,"message":str(e),"data": None}


def get_query(conn, query):
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        return {'status':True, 'message': '', 'data':data}
    except psycopg2.Error as e:
        return {"status": False,"message":str(e),"data": None}

def upsert_query(conn, query):
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        id = cursor.fetchone()[0]
        return {'status':True, 'message':'', 'data':id}
    except psycopg2.Error as e:
        return {'status':False, 'message':str(e), 'data':None}



