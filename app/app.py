import falcon
from services import db_services
from resources import wish, interface
from falcon_apispec import FalconPlugin


conn = db_services.connect()

api = falcon.API()
api.req_options.auto_parse_form_urlencoded = True

wishlist = wish.Wish(conn, db_services)
interface = interface.Interface()

api.add_route('/wishlist', wishlist, suffix='collection')
api.add_route('/wishlist/{id}', wishlist)
api.add_route('/wishlist/random', wishlist, suffix='random')
#api.add_route('/', interface)

