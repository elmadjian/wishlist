from falcon import testing
import app


class WishlistTestCase(testing.TestCase):
    
    def setUp(self):
        super(WishlistTestCase, self).setUp()
        self.app = app.api


class TestWishlistApp(WishlistTestCase):

    def test_post(self):
        doc = {
            'title': 'torta holandesa',
            'description': 'torta gorda com bolacha calipso',
            'owned': False,
            'link': ' ',
            'image': ' '
            }
        result = self.simulate_post('/wishlist', json=doc)
        self.assertEqual(result.json['status'], True)


    def test_incomplete_post(self):
        doc = {
            'title': 'beirute',
            'description': ' '
        }
        result = self.simulate_post('/wishlist', json=doc)
        self.assertEqual(result.json['status'], False)


    def test_get(self):
        result = self.simulate_get('/wishlist/1')
        self.assertEqual(result.json['status'], True)


    def test_unavailable_get(self):
        result = self.simulate_get('/wishlist/999')
        self.assertEqual(result.json['data'], [])


    def test_put(self):
        doc = {
            'title': 'testing put',
            'description': ' ',
            'owned': False,
            'link': ' ',
            'image': ' '
            }
        result = self.simulate_post('/wishlist', json=doc)
        idx = result.json['data']
        doc = {
            'title': 'mostarda',
            'owned': True
        }
        result = self.simulate_put('/wishlist/{}'.format(idx), json=doc)
        self.assertEqual(result.json['status'], True)


    def test_wrong_put(self):
        doc = {
            'name': 'mostarda',
            'owned': True
        }
        result = self.simulate_put('/wishlist/1', json=doc)
        self.assertEqual(result.json['status'], False)


    def test_delete(self):
        doc = {
            'title': 'testing delete',
            'description': ' ',
            'owned': False,
            'link': ' ',
            'image': ' '
            }
        result = self.simulate_post('/wishlist', json=doc)
        idx = result.json['data']
        result = self.simulate_delete('/wishlist/{}'.format(idx))
        self.assertEqual(result.json['message'], 'DELETE 1')


    def test_wrong_delete(self):
        result = self.simulate_delete('/wishlist/999')
        self.assertEqual(result.json['message'], 'DELETE 0')


    def test_get_all(self):
        result = self.simulate_get('/wishlist')
        self.assertEqual(result.json['status'], True)

    
    def test_random_get(self):
        result = self.simulate_get('/wishlist/random')
        self.assertEqual(result.json['status'], True)
        


    

